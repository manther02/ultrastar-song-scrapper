##### First of all, thank you for using my projects, if you want me to keep maintaining them here a way to show you support.
[🙏 DONATE](https://www.paypal.com/donate/?hosted_button_id=2WAMRESLC7YS2)

# UltraStar Song Scrapper

This script will scrap the website usdb.animux.de and try to download all the needed files to use in the game Ultrastar, if for some reason any of the 4 resources (lyrics, video, audio and cover) are missing the script will try to get it from external sources, like google or youtube.

## MAKE SURE YOU RUN ALL THE BELOW COMMANDS AT THE REPO BASE DIRECTORY

## Setup

To setup make sure you are in a linux based environment that uses the `apt` package manager, if you fill the requirements just use the below command, if not, install the packages and dependencies contained in the file.

> bash setup.sh


# Authentication

Login into usdb.animux.de with your account, press F12 to inspect the page, reload, check the first request headers and copy them into the `authentication` file.

It should look like this *(make sure you replace the ": " with "=" if needed*:

```
Accept=text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
Accept-Encoding=gzip, deflate
Accept-Language=en-US,en;q=0.9
Connection=keep-alive
Cookie=PHPSESSID=nbbb8e5bgig98ugdlb7fq9mie7; __utma=7495734.762168696.1674958910.1674958910.1674980798.2; __utmc=7495734; __utmz=7495734.1674958910.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmb=7495734.11.10.1674980798; __utmt=1
Host=usdb.animux.de
Upgrade-Insecure-Requests=1
User-Agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36
```

## Basic usage

To `try` to download all the database (Good Luck) just run the following command.

> python3 scrape.py

If you want to download with more specificity, you can set the `Artist` or the `Artist` and `Song Name` (only the `Song Name` is not supported).
`MAKE SURE YOU WRITE THE ARTIST AND SONG NAME AS WELL AS YOU CAN`

> python3 scrape.py "Artist Name"

> python3 scrape.py "Artist Name" "Song Name"

## Finished

After you have finished downloading everything, all the directories will be inside `./songs`, just copy and paste all of them inside you UltraStar songs directory.
And it's done!

### Thanks for reading 👌